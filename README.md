# FC pattern of obesity

Whole-brain functional connectivity correlates of obesity phenotypes

* **Please cite the paper if you use this code** \
*B.-y. Park, K. Byeon, M. J. Lee, C.-S. Chung, S.-H. Kim, F. Morys, B. Bernhardt, A. Dagher, and H. Park.* Whole-brain functional connectivity correlates of obesity phenotypes. *Human Brain Mapping* (2020). \
https://onlinelibrary.wiley.com/doi/full/10.1002/hbm.25167

# Prerequisite

* **MATLAB >= v.2016b** 

* **Preprocessing: FuNP_v >= 2.8** \
  Details of FuNP pipeline can be found at: https://gitlab.com/by9433/funp

* **Functional connectivity analysis: FSL >= v.5.0.8** \
  Download page of FSL: https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation

* **Construction of FC pattern requires Brainnetome atlas** \
  Download page of Brainnetome atlas: http://atlas.brainnetome.org/download.html

# Websites

* **LAB (CAMIN: Computational Analysis for Multimodal Integrative Neuroimaging):** https://www.caminlab.com/
* **LAB (MIPL: Medical Image Processing Lab):** https://mipskku.wixsite.com/mipl
* **LAB (MICA: Multimodal Imaging and Connectome Analysis):** http://mica-mni.github.io/