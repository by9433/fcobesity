%% Data preprocessing
% https://gitlab.com/by9433/funp
% FuNP (Fusion of Neuroimaging Preprocessing) pipelines: A fully automated preprocessing software for functional magnetic resonance imaging
% Frontiers in Neuroinformatics, 2019

%% FuNP: Volume T1
clear;clc

HeadPath = '~/FCobesity';     % Path that contains FuNP settings

load(strcat(HeadPath,'/FuNP_Volume_T1_settings.mat'));

for list = 1:size(T1_total_list, 1)
    in_list = T1_total_list(list,:);        %%% Change the data list
    
    preproc_volume_T1(in_list, reorient, orientation, mficor, skullremoval_T1,...
        regis_T1, standard_path, standard_name, standard_ext, dof, segmentation);
end

%% FuNP: Volume fMRI
clear;clc

HeadPath = '~/FCobesity';     % Path that contains FuNP settings

load(strcat(HeadPath,'/FuNP_Volume_fMRI_settings.mat'));

for list = 1:size(fMRI_total_list, 1)
    in_list_fMRI = fMRI_total_list(list,:);        %%% Change the data list
    in_list_T1 = T1_total_list(list,:);               %%% Change the data list
    
    preproc_volume_fMRI(in_list_fMRI, reorient, orientation, delnvol, VOLorSEC, VOLorSEC_enterval,...
        motscrub, FDthresh_enterval, motcor, stcor, stcor2, stcor_opt, slice_order_file,...
        discor, reverse_path, reverse_name, reverse_ext, total_readout_time, skullremoval_fMRI, intnorm,...
        regis_fMRI, T1_total_list, dof1, standard_path, standard_name, standard_ext, dof2,...
        nvremove, FIX_dim, FIX_train, tempfilt, filter_kind, lpcutoff, hpcutoff, smoothing, fwhm_val);
end

%% Functional connectivity analysis
% FSL is required

%% Extract time series from the preprocessed fMRI using Brainnetome atlas
clear;clc

HeadPath = '~/FCobesity';
DataPath = strcat(HeadPath,'/DATA');
AtlasPath = strcat(HeadPath,'/BrainnetomeAtlas');
func_name = 'Smooth_rest';    %%% Change the name of the input data (output of preprocessing)

% Prepare atlas
if exist('/tmp/tempAtlas')
    system(['rm -rf /tmp/tempAtlas']);
end
mkdir('/tmp/tempAtlas');
in_atlas = load_nii(strcat(AtlasPath,'/BNA_2mm'));          %%% Change if other dimension is used
atlas_idx = unique(in_atlas.img(:));
if atlas_idx(1) == 0
    atlas_idx(1,:) = [];
end
NumROI = size(atlas_idx,1);
for nr = 1:NumROI
    mask_idx = find(in_atlas.img(:) == atlas_idx(nr));
    temp_img = zeros(size(in_atlas.img));
    temp_img(mask_idx) = 1;
    temp_nii = in_atlas;
    temp_nii.img = temp_img;
    
    if nr < 10
        temp_nr = strcat('00',int2str(nr));
    elseif nr < 100
        temp_nr = strcat('0',int2str(nr));
    elseif nr < 1000
        temp_nr = int2str(nr);
    end
    save_nii(temp_nii, strcat('/tmp/tempAtlas/atlas_',temp_nr,'.nii'));
    
    system(strcat([ '3dresample -orient RPI ',...
        ' -prefix /tmp/tempAtlas/RPI_atlas_',temp_nr,'.nii ',...
        ' -inset /tmp/tempAtlas/atlas_',temp_nr,'.nii']));
end


% Extract time series
Dlist = dir(DataPath);
IClist = dir(strcat('/tmp/tempAtlas/RPI_atlas_*.nii'));
for list1 = 3:length(Dlist)
    disp(strcat(['list = ',int2str(list1),' -- ',Dlist(list1).name]));
    
    IndPath = strcat(DataPath,'/',Dlist(list1).name);
    mkdir(strcat(IndPath,'/FC'));
    
    for list2 = 1:length(IClist)
        disp(strcat(['       Atlas = ',int2str(list2),' -- ',IClist(list2).name]));
        
        system(strcat(['fslmeants -i ',IndPath,'/func_results/',func_name,' '...
            ' -m /tmp/tempAtlas/',IClist(list2).name,...
            ' -o ',IndPath,'/FC/IC',int2str(list2),'.txt']));
    end
    
    temp = importdata(strcat(IndPath,'/FC/IC1.txt'));
    sub_ts = zeros(size(temp,1), length(IClist));
    for list2 = 1:length(IClist)
        temp = importdata(strcat(IndPath,'/FC/IC',int2str(list2),'.txt'));
        sub_ts(:,list2) = temp;
    end
    
    dlmwrite(strcat(IndPath,'/FC/ts_BNA.txt'), sub_ts);
    system(strcat(['rm -rf ',IndPath,'/FC/IC*']));
end

if exist('/tmp/tempAtlas') ~= 0
    system(strcat(['rm -rf /tmp/tempAtlas']));
end

%% Calculate DC values
clear;clc

HeadPath = '~/FCobesity';
DataPath = strcat(HeadPath,'/DATA');

TR = 0.735;     %%% Change the TR according to your own data
beta = 6;  % scale-free index

rho = 0.5;  % UKBiobank style

flist = dir(DataPath);
for list = 3:length(flist)
    disp(strcat(['list = ',int2str(list),' -- ',flist(list).name]));
    
    IndPath = strcat(DataPath,'/',flist(list).name);
    
    
    %%%%% Construct connectivity matrix %%%%%
    grotALL=load(strcat(IndPath,'/FC/ts_BNA.txt'));
    gn=size(grotALL,1);
    
    grot=grotALL(1:gn,:);
    grot=grot-repmat(mean(grot),size(grot,1),1); % demean
    grot=grot/std(grot(:)); % normalise whole subject stddev
    TS=grot;
    
    N=size(TS,2);
    
    % Full correlation
    grot = TS;
    grot=corr(grot);
    grot(eye(N)>0)=0;
    netmats1(1,:)=reshape(grot,1,N*N);
    
    % Partial correlation with ridge regression
    grot=TS;
    grot=cov(grot);
    grot=grot/sqrt(mean(diag(grot).^2));
    grot=-inv(grot+rho*eye(N));
    grot=(grot ./ repmat(sqrt(abs(diag(grot))),1,N)) ./ repmat(sqrt(abs(diag(grot)))',N,1);  grot(eye(N)>0)=0;
    netmats2(1,:)=reshape(grot,1,N*N);
    
    % Make correlation matrix
    L = reshape(netmats1,N,N);
    H = reshape(netmats2,N,N);
    ConnMatR = tril(L) + triu(H);
    
    
    
    %%%%% Calculate network parameters %%%%%
    % Construct real network
    % Full correlation: tril / Partial correlation: triu
    RealNet_full = tril(ConnMatR);             RealNet_full = RealNet_full + RealNet_full';
    RealNet_partial = triu(ConnMatR);       RealNet_partial = RealNet_partial + RealNet_partial';
    
    RealNet_wei_full = ((RealNet_full+1)/2).^beta;
    RealNet_weiZ_full = .5*log((1+RealNet_wei_full)./(1-RealNet_wei_full));
    RealNet_wei_partial = ((RealNet_partial+1)/2).^beta;
    RealNet_weiZ_partial = .5*log((1+RealNet_wei_partial)./(1-RealNet_wei_partial));
    
    
    
    % Calculate real network parameters
    DC_full = sum(RealNet_weiZ_full);
    DC_partial = sum(RealNet_weiZ_partial);
    
    save(strcat(IndPath,'/FC/FC_BNA.mat'), 'ConnMatR', 'DC_full', 'DC_partial');
end

%% Calculate FC pattern score
clear;clc

HeadPath = '~/FCobesity';
DataPath = strcat(HeadPath,'/DATA');

FCpattern = [0;0;0;0;0;-0.983400000000000;-1.47370000000000;-3.39760000000000;0;0;0;-0.842600000000000;1.74260000000000;1.77930000000000;-1.05810000000000;-0.793700000000000;0;0;0;0;0;-0.951900000000000;0;-2.99440000000000;-3.45780000000000;0;-2.63590000000000;0;2.40930000000000;0;0;0;0.361400000000000;0;0;2.28360000000000;0;0;0;0;6.59490000000000;0;0;0;0;3.45660000000000;1.74990000000000;1.82110000000000;-1.76540000000000;0;-2.07340000000000;5.10500000000000;-0.479000000000000;0;0;-0.433500000000000;-4.80430000000000;0;-0.577300000000000;-1.11700000000000;-6.95450000000000;-0.964700000000000;2.24130000000000;0;-1.74890000000000;0;0;0;0;0;0;0.805500000000000;-1.41680000000000;0.137300000000000;4.45060000000000;0;-0.954500000000000;5.40970000000000;-3.89660000000000;0;0;-0.999400000000000;0;0;1.51760000000000;2.17290000000000;0;0;0;2.29250000000000;0;-3.47040000000000;-3.16210000000000;0;0;0;0;0;0;-4.90560000000000;-0.657000000000000;-4.39970000000000;-8.13620000000000;-6.59470000000000;0;-1.78900000000000;0;-1.11770000000000;0;0;0;-0.866700000000000;1.08070000000000;0;0;0;0;-4.15070000000000;-3.20240000000000;0;0;0;-5.45330000000000;0;4.66830000000000;0;-1.89530000000000;0;7.34370000000000;1.54810000000000;0;0;1.82180000000000;0;0;1.60130000000000;0;0.618300000000000;0.143200000000000;0;1.12860000000000;1.39650000000000;-2.11450000000000;0.684000000000000;2.12210000000000;0;0;0;-0.543600000000000;0;0;0;-3.27480000000000;0;0;0.557100000000000;-0.445200000000000;0;-1.31660000000000;0;-2.30390000000000;0;0;0;0;3.08580000000000;0;0;0;0;0;0;0;0;-0.0332000000000000;0;0;0;0;0;-0.277700000000000;0;0;0;0;0.245000000000000;4.66080000000000;0;0;0;0;-1.20240000000000;0.151200000000000;0;0;6.97930000000000;-0.104300000000000;-0.695600000000000;-1.26560000000000;0.321700000000000;0.401600000000000;0;1.88740000000000;0;0;1.24960000000000;0;-1.42310000000000;-0.321400000000000;0;0;0;0;-0.384300000000000;0;-1.73400000000000;0;0;-0.127800000000000;0;6.22720000000000;0;3.47680000000000;0;0;-1.16580000000000;0;-2.10130000000000;0;0;1.08060000000000;-0.276800000000000;0;0;0;-4.61710000000000;-5.42460000000000;0;0;0;0;0;-2.33000000000000;0.0728000000000000;0;-0.0772000000000000];

flist = dir(DataPath);
for list = 3:length(flist)
    disp(strcat(['list = ',int2str(list),' -- ',flist(list).name]));
    
    IndPath = strcat(DataPath,'/',flist(list).name);
    load(strcat(IndPath,'/FC/FC_BNA.mat'));
    
    FCpattern_score = DC_partial * FCpattern;
    
    save(strcat(IndPath,'/FC/FCpattern_score.mat'), 'FCpattern_score');
end



